const DEFAULT_SEPARATOR = /[\n,]/;
const CUSTOM_SEPARATOR = /^\/\/(\[?(?:.*)?\]?)\n/;
const DEFAULT_REPL_SEPARATOR = ',';
const NUM_LIMIT = 1e3;

function extractNumber(raw) {
    const num = ~~(+raw);
    if (num < 0) {
        throw new Error(`${num}`);
    }
    return num;
}

function normalizeNumber(num) {
    return num <= NUM_LIMIT ? num : 0;
}

function sum(items) {
    return items.reduce((sum, currentCharNum) => {
        const num = extractNumber(currentCharNum);
        return sum + normalizeNumber(num);
    }, 0);
}

function removeSepratorMetadata(input) {
    return input.replace(CUSTOM_SEPARATOR, '');
}

function normalizeSeparator(input) {
    const customPossibleSeparator = (input.match(CUSTOM_SEPARATOR) || [])[1];
    if (customPossibleSeparator) {
        const separators = customPossibleSeparator.replace(/^\[|\]$/g, '').split('][');
        separators.forEach(sep => {
            input = input.split(sep).join(DEFAULT_REPL_SEPARATOR);
        })
    }
    return input;
}

module.exports = function(input) {
    input = normalizeSeparator(input);
    input = removeSepratorMetadata(input);
    const items = input.split(DEFAULT_SEPARATOR);
    return sum(items);
};