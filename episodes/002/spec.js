const assert = require('assert');
const solution = require('./solution');

describe('Digital craftsmen screencast - episode 002', () => {
    
    it ('returns 0 on empty string', () => {
        assert.equal(solution(''), 0);
    });

    it('return a number when single number is given', ()=>{
        assert.equal(solution('1,'), 1);
    });

    it('returns a sum of all the numbers', () => {
        assert.equal(solution('1,2'), 3);
    });

    it('should accept new lines as number delimiters', () => {
        assert.equal(solution("1\n3,2"), 6);
    });

    [
        ';', 
        'x', 
        '*', 
        '7'
    ].forEach(customDelimiter => {
        it(`should support custom delimiters "${customDelimiter}"`, () => {
            const defineDelimiter = `//${customDelimiter}\n`; //*\n1*2
            assert.equal(solution(`${defineDelimiter}1${customDelimiter}2`), 3);
        });
    });

    it(`should not accept negative numbers`, () => {
        assert.throws(() => solution(`-1,1`), Error, '-1');
    });

    it('ignores numbers bigger than 1000', () => {
        assert.equal(solution('1, 1001'), 1);
    });

    it('supports long delimiters', () => {
        const customDelimiter = '***';
        const defineDelimiter = `//[${customDelimiter}]\n`;
        assert.equal(solution(`${defineDelimiter}1${customDelimiter}2`), 3);
    });

    it('should support multiple custom delimiters', () => {
        const customDelimiter1 = '***';
        const customDelimiter2 = '^^';
        const defineDelimiter = `//[${customDelimiter1}][${customDelimiter2}]\n`;
        assert.equal(solution(`${defineDelimiter}1${customDelimiter1}2${customDelimiter2}3`), 6);
    })
    
});