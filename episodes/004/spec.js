const assert = require('assert');
const solution = require('./solution');

describe('Digital craftsmen screencast - episode 004', () => {

    [
        // 1-10:
        // 1,2,3 -> no digit
        // 4 -> has I
        // 5,6,7,8 -> no digit
        // 9 -> has I

        // 40 XL
        // 400 I*100 (C) D -> CD
        // 456 -> 400 + 50 + 6
        // 400 -> CD
        // 50 -> L
        // 6 -> VI
        // CDLVI


        // [1, 'I'],
        // [2, 'II'],
        // [3, 'III'],
        // [5, 'V'],
        // [6, 'VI'],
        // [10, 'X'],
        // [24, 'XXIV'],
        // [4, 'IV'],
        // [9, 'IX'],
        // [50, 'L'],
        // [100, 'C'],
        // [90, 'XC'],
        // [456, 'CDLVI'],
        [1990, 'MCMXC'],
        // [2751, 'MMDCCLI']
    ].forEach(([val, expected]) => {
        it(`converts ${val} into ${expected}`, () => {
            assert.equal(expected, solution(val));
        });
    });

});