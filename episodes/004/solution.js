// 1 - Write only enough of a unit test to fail.
// 2 - Write only enough production code to make the failing unit test pass.


const replacers = {
    1: 'I',
    5: 'V',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M'
};

function transformNumberPart(digit, magnitude) {
    console.log('Digit: ', digit);
    console.log('Magnitude: ', magnitude);
    magnitude /= 10;
    digit = digit / magnitude;
    if (digit < 4) {
        return replacers[1 * magnitude].repeat(digit);
    } else if (digit === 4) {
        return replacers[1 * magnitude] + replacers[5 * magnitude];
    } else if (digit > 4 && digit < 9) {
        return replacers[5 * magnitude] + replacers[1 * magnitude].repeat(digit - 5);
    } else if (digit === 9) {
        return replacers[1 * magnitude] + replacers[10 * magnitude];
    }
}

module.exports = function (input) {
    // let originalInput = input;
    // let magnitude = 1;
    // while (input > 0) {
    //     magnitude *= 10;
    //     input = Math.floor(input / 10);
    // }
    // magnitude /= 10;
    let output = '';
    let magnitude = 10;

    while (input % magnitude > 0) {
        const digit = input % magnitude;
        input -= digit;
        output = transformNumberPart(digit, magnitude) + output;
        magnitude *= 10;
    }

    return output;
};