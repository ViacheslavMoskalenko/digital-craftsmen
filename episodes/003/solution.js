const color = {
    RED: 'red',
    BLUE: 'blue',
    GREEN: 'green'
}

const matches = {
    WRONG_PLACE: 'wp',
    EXACT_PLACEMENT: 'ep'
}

/*
 * See the original game at: https://en.wikipedia.org/wiki/Mastermind_(board_game)
 */

class Game {
    constructor(...colors) {
        this._colors = colors;
        this._memo = [];
    }

    _alreadyChecked(index) {
        return this._memo.includes(index);
    }
    
    _neverChecked(index) {
        return !this._alreadyChecked(index);
    }

    _markAsChecked(index) {
        this._memo.push(index);
    }

    _classifyMatch(color, index, colors) {
        if (this._neverChecked(index) && this._colors[index] === color) {
            this._markAsChecked(index);

            return matches.EXACT_PLACEMENT;
        }

        for(let i = 0; i < this._colors.length; i++){
            if (this._alreadyChecked(i) || colors[i] === color)
                continue;
            
            if( this._colors[i] === color){
                this._markAsChecked(i);
                return matches.WRONG_PLACE;
            }
        }
    }
 
    check(...colorsToTest) {
        return colorsToTest
            .map((color, ix) => this._classifyMatch(color, ix, colorsToTest))
            .filter(Boolean);
    }

}

const gameInfo = {
    Game,
    color,
    matches
};

module.exports = gameInfo;