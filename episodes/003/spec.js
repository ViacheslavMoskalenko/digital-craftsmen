const assert = require('assert');
const solution = require('./solution');

const { RED, BLUE, GREEN } = solution.color;
const { WRONG_PLACE, EXACT_PLACEMENT } = solution.matches;

describe('Digital craftsmen screencast - episode 003', () => {

    it('should return empty array when no matches', () => {
        const game = new solution.Game(RED, RED, RED, RED);

        assert.deepEqual(game.check(BLUE, BLUE, BLUE, BLUE), []);
    });

    it('should return one wrong placement match', () => {
        const game = new solution.Game(RED, RED, RED, GREEN);

        const result = game.check(GREEN, BLUE, BLUE, BLUE);

        assert.deepEqual(result, [WRONG_PLACE]);
    });

    it('should return two wrong placement matches', () => {
        const game = new solution.Game(RED, RED, GREEN, GREEN);

        const result = game.check(GREEN, GREEN, BLUE, BLUE);

        assert.deepEqual(result, [WRONG_PLACE, WRONG_PLACE]);
    });

    it('should return one exact placement match', () => {
        const game = new solution.Game(RED, RED, RED, GREEN);

        const result = game.check(BLUE, BLUE, BLUE, GREEN);

        assert.deepEqual(result, [EXACT_PLACEMENT]);
    });
    
    it('should not count twice checked color', () => {
        const game = new solution.Game(RED, RED, GREEN, GREEN);

        const result = game.check(GREEN, BLUE, BLUE, BLUE);

        assert.deepEqual(result, [WRONG_PLACE]);
    });

    it('should not count twice existing color', () => {
        const game = new solution.Game(RED, RED, RED, GREEN);

        const result = game.check(GREEN, GREEN, BLUE, BLUE);

        assert.deepEqual(result, [WRONG_PLACE]);
    });
    
    it('should take exact placement as top priority match', () => {
        const game = new solution.Game(RED, RED, GREEN, GREEN);

        const result = game.check(GREEN, BLUE, GREEN, BLUE);

        assert.deepEqual(result, [WRONG_PLACE, EXACT_PLACEMENT]);
    });
    
    it('should return all matches', () => {
        const game = new solution.Game(RED, RED, RED, RED);

        const result = game.check(RED, RED, RED, RED);

        assert.deepEqual(result, [EXACT_PLACEMENT, EXACT_PLACEMENT, EXACT_PLACEMENT, EXACT_PLACEMENT]);
    });

});