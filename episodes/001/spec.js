const assert = require('assert');
const solution = require('./solution');

describe('Digital craftsmen screencast', () => {

    [
        [0, 0], // 0 0 0 0 -> 0
        [1, 0], // 0 0 0 1 -> 1
        [6, 0], // 0 1 1 0 -> 6   
        [7, 0], // 0 1 1 1 -> 7
        [5, 1], // 0 1 0 1 -> 5
        [10, 1],// 1 0 1 0 -> 10
        [11, 1], // 1 0 1 1 -> 11
        [77, 2], // 1 0 0 1 1 0 1 -> 77
        [67207659, 9], // 100000000011000000111101011 -> 77
    ].forEach(([given, expected]) => {
        it(`should return ${expected} when ${given} solution`, () => {
            assert.equal(expected, solution(given));
        });
    });

    it('should return num of zeroes in binary representation surrounded by ones', () => {
        assert.equal(2, solution(9));
    });

    it ('should return max number of zeroes surrounded with ones', () => {
        assert.equal(2, solution(37));
    })

});