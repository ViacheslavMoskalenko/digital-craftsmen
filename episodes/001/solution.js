// 1 - Write only enough of a unit test to fail.
// 2 - Write only enough production code to make the failing unit test pass.

const toBin = num => num.toString(2);

const cutZerosFromEnd = input => {
    if (input) while ((input & 1) === 0) input >>= 1;
    return input;
};

const cutOnes = input => {
    while ((input & 1) === 1) input >>= 1;
    return input;
};

const countZeros = input => {
    let c = 0;
    while (input && (input & 1) !== 1) {
        c++;
        input >>= 1;
    }
    return c;
};

module.exports = function(input) {
    // step 1: - cut all the zero at the end
    // step 2: - cut all the ones until the first zero
    // step 3: - count all the zeros between ones
    // Estimates: time O(lgn), space O(1)
    input = cutZerosFromEnd(input);
    let maxZeroesBetweenOnes = 0;
    while(input !== 0) {
        input = cutOnes(input);
        const zeroesInInterval = countZeros(input);
        maxZeroesBetweenOnes = Math.max(maxZeroesBetweenOnes, zeroesInInterval);
        input >>= zeroesInInterval;
    }
    return maxZeroesBetweenOnes;
};